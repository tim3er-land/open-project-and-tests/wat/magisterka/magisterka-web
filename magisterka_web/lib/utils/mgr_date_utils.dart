class MgrDateUtils {
  static String parseTime(String date) {
    if (date.isEmpty) return "";
    DateTime parsedDate = DateTime.parse(date);
    return getStringValue(parsedDate.day) +
        "-" +
        getStringValue(parsedDate.month) +
        "-" +
        getStringValue(parsedDate.year) +
        " " +
        getStringValue(parsedDate.hour) +
        ":" +
        getStringValue(parsedDate.minute);
  }

  static String getStringValue(int i) {
    if (i < 10) {
      return "0" + i.toString();
    }
    return i.toString();
  }
}
