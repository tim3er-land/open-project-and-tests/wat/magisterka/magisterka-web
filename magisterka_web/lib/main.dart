import 'package:flutter/material.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:magisterka_web/page/agent/agent_add_mofidy_page.dart';
import 'package:magisterka_web/page/agent/agent_data_page.dart';
import 'package:magisterka_web/page/agent/agent_info_page.dart';
import 'package:magisterka_web/page/home_page.dart';
import 'package:magisterka_web/page/login_page.dart';
import 'package:magisterka_web/page/register_page.dart';
import 'package:magisterka_web/page/rule/rule_add_mofidy_page.dart';
import 'package:magisterka_web/page/welcome_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<void> main() async {
  final prefs = await SharedPreferences.getInstance();
  String? token = prefs.getString('token');
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    title: 'Magisterka',
    initialRoute:
        // "/",
        token == null || JwtDecoder.isExpired(token) ? "/" : '/home',
    routes: {
      "/login": (context) => LoginPage(),
      "/register": (context) => RegisterPage(),
      "/": (context) => WelcomePage(),
      "/home": (context) => HomePage(),
      "/agent/data": (context) =>
          AgentDataPage(ModalRoute.of(context)!.settings.arguments as Map),
      "/agent/info": (context) =>
          AgentInfoPage(ModalRoute.of(context)!.settings.arguments as Map),
      "/agent/add": (context) => AgentAddModifyPage(
          ModalRoute.of(context)!.settings.arguments as Map, false),
      // "/agent/edith": (context) => AgentAddModifyPage(
      //     ModalRoute.of(context)!.settings.arguments as Map, true),
      "/rule/add": (context) => RuleAddModifyPage(
          ModalRoute.of(context)!.settings.arguments as Map, false),
      "/rule/edith": (context) => RuleAddModifyPage(
          ModalRoute.of(context)!.settings.arguments as Map, true),
    },
  ));
}
