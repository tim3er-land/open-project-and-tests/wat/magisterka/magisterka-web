import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'basic_rest.dart';

class AuthRest extends BasicRest {
  dynamic login(_password, _login, BuildContext context) async {
    var _body = {'login': _login, 'password': _password};
    try {
      Response response = await Dio().post(
        'http://localhost:8090/login',
        data: jsonEncode(_body),
        options: Options(
          headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            'Content-Type': 'application/json; charset=UTF-8',
            "Access-Control_Allow_Origin": "*"
          },
        ),
      );
      final prefs = await SharedPreferences.getInstance();
      await prefs.setString('token', response.data['token']);
      Navigator.pushReplacementNamed(context, '/home');
    } on DioError catch (e) {
      if (e.response?.data != null)
        showDialog(
          context: context,
          builder: (BuildContext context) => buildPopupDialog(
              context, e.response?.data['code'], e.response?.data['message']),
        );
    }
  }
}
