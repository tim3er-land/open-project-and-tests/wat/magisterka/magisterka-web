import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BasicRest {
  Widget buildPopupDialog(BuildContext context, String _code, String _message) {
    return new AlertDialog(
      title: const Text('Rest Error'),
      content: new Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(_code),
          Text('Code',
              style: TextStyle(color: Colors.grey.shade600, fontSize: 10)),
          SizedBox(
            height: 10,
          ),
          Text(_message),
          Text('Message',
              style: TextStyle(color: Colors.grey.shade600, fontSize: 10)),
        ],
      ),
      actions: <Widget>[
        new OutlinedButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: const Text('Close'),
        ),
      ],
    );
  }

  Widget buildPopupDialogWithErrors(BuildContext context, String _code,
      String _message, List<dynamic> _errors) {
    return new AlertDialog(
      title: const Text('Rest Error'),
      content: new Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(_code),
          Text('Code',
              style: TextStyle(color: Colors.grey.shade600, fontSize: 10)),
          SizedBox(
            height: 10,
          ),
          Text(_message),
          Text('Message',
              style: TextStyle(color: Colors.grey.shade600, fontSize: 10)),
          if (_errors != null)
            Column(
              children: _errors.map((e) {
                return Row(
                  children: [
                    Column(
                      children: [
                        Text(e['message']),
                        Text('Message',
                            style: TextStyle(
                                color: Colors.grey.shade600, fontSize: 10)),
                      ],
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Column(
                      children: [
                        Text(e['code']),
                        Text('Dode',
                            style: TextStyle(
                                color: Colors.grey.shade600, fontSize: 10)),
                      ],
                    )
                  ],
                );
              }).toList(),
            )
        ],
      ),
      actions: <Widget>[
        new OutlinedButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: const Text('Close'),
        ),
      ],
    );
  }

  dynamic checkErrorOrAuth(DioError e, BuildContext context) async {
    if (e.response?.data != null) {
      if (e.response?.statusCode == 403 || e.response?.statusCode == 401) {
        final prefs = await SharedPreferences.getInstance();
        await prefs.remove('token');
        Navigator.pushReplacementNamed(context, '/');
        return;
      }
      showDialog(
        context: context,
        builder: (BuildContext context) => buildPopupDialog(
            context, e.response?.data['code'], e.response?.data['message']),
      );
    }
  }

  dynamic checkErrorOrAuthWithErrors(DioError e, BuildContext context) async {
    if (e.response?.data != null) {
      if (e.response?.statusCode == 403 || e.response?.statusCode == 401) {
        final prefs = await SharedPreferences.getInstance();
        await prefs.remove('token');
        Navigator.pushReplacementNamed(context, '/');
        return;
      }
      if (e.response?.data['errors'] == null)
        showDialog(
          context: context,
          builder: (BuildContext context) => buildPopupDialog(
              context, e.response?.data['code'], e.response?.data['message']),
        );
      else
        showDialog(
          context: context,
          builder: (BuildContext context) => buildPopupDialogWithErrors(
              context,
              e.response?.data['code'],
              e.response?.data['message'],
              e.response?.data['errors']),
        );
    }
  }
}
