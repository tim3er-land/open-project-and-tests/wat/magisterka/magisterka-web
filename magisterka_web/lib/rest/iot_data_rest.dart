import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../dto/AgentDataDto.dart';
import '../dto/IotSummaryDto.dart';
import 'basic_rest.dart';

class IotDataRest extends BasicRest {
  Future<List<AgentDataDto>> getAgentData(
      BuildContext context, String agentUid) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      String? token = prefs.getString('token');
      Response response = await Dio().get(
        'http://localhost:8093/agent/data/' + agentUid,
        // 'http://localhost:8098/agent/data/' + agentUid,
        options: Options(
          headers: {
            'Authorization': 'Bearer ' + token!,
            "Accept": "application/json",
            "Content-Type": "application/json",
            'Content-Type': 'application/json; charset=UTF-8',
            "Access-Control_Allow_Origin": "*"
          },
        ),
      );
      Iterable l = response.data;
      return List<AgentDataDto>.from(
          l.map((model) => AgentDataDto.fromJson(model)));
    } on DioError catch (e) {
      showDialog(
        context: context,
        builder: (BuildContext context) => buildPopupDialog(
            context, e.response?.data['code'], e.response?.data['message']),
      );
      return <AgentDataDto>[];
    }
  }

  Future<List<AgentDataDto>> getAgentDataRaw(
      BuildContext context, String agentUid) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      String? token = prefs.getString('token');
      Response response = await Dio().get(
        'http://localhost:8093/agent/data/' + agentUid + '/raw',
        // 'http://localhost:8098/agent/data/' + agentUid + '/raw',
        options: Options(
          headers: {
            'Authorization': 'Bearer ' + token!,
            "Accept": "application/json",
            "Content-Type": "application/json",
            'Content-Type': 'application/json; charset=UTF-8',
            "Access-Control_Allow_Origin": "*"
          },
        ),
      );
      Iterable l = response.data;
      return List<AgentDataDto>.from(
          l.map((model) => AgentDataDto.fromJson(model)));
    } on DioError catch (e) {
      showDialog(
        context: context,
        builder: (BuildContext context) => buildPopupDialog(
            context, e.response?.data['code'], e.response?.data['message']),
      );
      return <AgentDataDto>[];
    }
  }

  Future<IotSummaryDto> getIotSummary(BuildContext context) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      String? token = prefs.getString('token');
      Response response = await Dio().get(
        'http://localhost:8093/iot/summary',
        options: Options(
          headers: {
            'Authorization': 'Bearer ' + token!,
            "Accept": "application/json",
            "Content-Type": "application/json",
            'Content-Type': 'application/json; charset=UTF-8',
            "Access-Control_Allow_Origin": "*"
          },
        ),
      );
      return IotSummaryDto.fromJson(response.data);
    } on DioError catch (e) {
      showDialog(
        context: context,
        builder: (BuildContext context) => buildPopupDialog(
            context, e.response?.data['code'], e.response?.data['message']),
      );
      return IotSummaryDto(0, 0, 0, 0, 0, {});
    }
  }

  Future<IotSummaryDto> sendDataToAgent(
      BuildContext context, String topic, String data) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      String? token = prefs.getString('token');
      Response response = await Dio().put(
        'http://localhost:8093/agent/send',
        data: {'topic': topic, 'data': data},
        options: Options(
          headers: {
            'Authorization': 'Bearer ' + token!,
            "Accept": "application/json",
            "Content-Type": "application/json",
            'Content-Type': 'application/json; charset=UTF-8',
            "Access-Control_Allow_Origin": "*"
          },
        ),
      );
      return IotSummaryDto.fromJson(response.data);
    } on DioError catch (e) {
      showDialog(
        context: context,
        builder: (BuildContext context) => buildPopupDialog(
            context, e.response?.data['code'], e.response?.data['message']),
      );
      return IotSummaryDto(0, 0, 0, 0, 0, {});
    }
  }
}
