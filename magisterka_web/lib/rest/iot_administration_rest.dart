import 'dart:typed_data';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../dto/AgentExtDto.dart';
import '../dto/AgentTule.dart';
import 'basic_rest.dart';

class IotAdministrationRest extends BasicRest {
  Future<List<AgentExtDto>> getUserIotAgents(BuildContext context) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      String? token = prefs.getString('token');
      Response response = await Dio().get(
        'http://localhost:8094/iot/agent',
        options: Options(
          headers: {
            'Authorization': 'Bearer ' + token!,
            "Accept": "application/json",
            "Content-Type": "application/json",
            'Content-Type': 'application/json; charset=UTF-8',
            "Access-Control_Allow_Origin": "*"
          },
        ),
      );
      Iterable l = response.data;
      return List<AgentExtDto>.from(
          l.map((model) => AgentExtDto.fromJson(model)));
    } on DioError catch (e) {
      await checkErrorOrAuth(e, context);
      return <AgentExtDto>[];
    }
  }

  Future<List<AgentRule>> getUserRule(BuildContext context) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      String? token = prefs.getString('token');
      Response response = await Dio().get(
        'http://localhost:8094/iot/rule',
        options: Options(
          headers: {
            'Authorization': 'Bearer ' + token!,
            "Accept": "application/json",
            "Content-Type": "application/json",
            'Content-Type': 'application/json; charset=UTF-8',
            "Access-Control_Allow_Origin": "*"
          },
        ),
      );
      Iterable l = response.data;
      return List<AgentRule>.from(l.map((model) => AgentRule.fromJson(model)));
    } on DioError catch (e) {
      await checkErrorOrAuth(e, context);
      return <AgentRule>[];
    }
  }

  Future<dynamic> getAgentInfo(BuildContext context, String agentUid) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      String? token = prefs.getString('token');
      Response response = await Dio().get(
        'http://localhost:8094/iot/agent/' + agentUid,
        options: Options(
          headers: {
            'Authorization': 'Bearer ' + token!,
            "Accept": "application/json",
            "Content-Type": "application/json",
            'Content-Type': 'application/json; charset=UTF-8',
            "Access-Control_Allow_Origin": "*"
          },
        ),
      );
      return response.data;
    } on DioError catch (e) {
      await checkErrorOrAuth(e, context);
    }
  }

  Future<dynamic> deleteUserRule(BuildContext context, String ruleUid) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      String? token = prefs.getString('token');
      Response response = await Dio().delete(
        'http://localhost:8094/iot/rule/' + ruleUid,
        options: Options(
          headers: {
            'Authorization': 'Bearer ' + token!,
            "Accept": "application/json",
            "Content-Type": "application/json",
            'Content-Type': 'application/json; charset=UTF-8',
            "Access-Control_Allow_Origin": "*"
          },
        ),
      );
    } on DioError catch (e) {
      await checkErrorOrAuth(e, context);
    }
  }

  Future<dynamic> deleteAgent(BuildContext context, String agentUid) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      String? token = prefs.getString('token');
      Response response = await Dio().delete(
        'http://localhost:8094/iot/agent/' + agentUid,
        options: Options(
          headers: {
            'Authorization': 'Bearer ' + token!,
            "Accept": "application/json",
            "Content-Type": "application/json",
            'Content-Type': 'application/json; charset=UTF-8',
            "Access-Control_Allow_Origin": "*"
          },
        ),
      );
    } on DioError catch (e) {
      await checkErrorOrAuth(e, context);
    }
  }

  Future<dynamic> addNewAgent(BuildContext context, Map dataJson) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      String? token = prefs.getString('token');
      Response response = await Dio().put(
        'http://localhost:8094/iot/agent',
        data: dataJson,
        options: Options(
          headers: {
            'Authorization': 'Bearer ' + token!,
            "Accept": "application/json",
            "Content-Type": "application/json",
            'Content-Type': 'application/json; charset=UTF-8',
            "Access-Control_Allow_Origin": "*"
          },
        ),
      );
      Navigator.pop(context);
    } on DioError catch (e) {
      await checkErrorOrAuth(e, context);
    }
  }

  Future<dynamic> addNewRule(BuildContext context, Map dataJson) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      String? token = prefs.getString('token');
      Response response = await Dio().put(
        'http://localhost:8094/iot/rule',
        // 'http://localhost:8060/iot/rule',
        data: dataJson,
        options: Options(
          headers: {
            'Authorization': 'Bearer ' + token!,
            "Accept": "application/json",
            "Content-Type": "application/json",
            'Content-Type': 'application/json; charset=UTF-8',
            "Access-Control_Allow_Origin": "*"
          },
        ),
      );
      Navigator.pop(context);
    } on DioError catch (e) {
      await checkErrorOrAuthWithErrors(e, context);
    }
  }

  Future<dynamic> modifyAgentRule(BuildContext context, Map dataJson,
      String ruleUid) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      String? token = prefs.getString('token');
      Response response = await Dio().post(
        'http://localhost:8094/iot/rule/' + ruleUid,
        data: dataJson,
        options: Options(
          headers: {
            'Authorization': 'Bearer ' + token!,
            "Accept": "application/json",
            "Content-Type": "application/json",
            'Content-Type': 'application/json; charset=UTF-8',
            "Access-Control_Allow_Origin": "*"
          },
        ),
      );
      Navigator.pop(context);
    } on DioError catch (e) {
      await checkErrorOrAuth(e, context);
    }
  }

  Future<String> getAgentZip(BuildContext context, String agentUid) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      String? token = prefs.getString('token');
      http.Response response = await http.get(
          Uri.http(
              // 'localhost:8060', '/iot/agent/' + agentUid + '/zip'),
              'localhost:8094', '/iot/agent/' + agentUid + '/zip'),
          headers: {
            'Authorization': 'Bearer ' + token!,
            "Accept": "application/json",
            "Content-Type": "application/json",
            'Content-Type': 'application/json; charset=UTF-8',
            "Access-Control_Allow_Origin": "*"
          }
      );
      return response.body;
    } on DioError catch (e) {
      await checkErrorOrAuth(e, context);
    }
    return "  ";
  }
}
