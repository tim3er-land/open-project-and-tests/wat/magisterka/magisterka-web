import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'basic_rest.dart';

class UserAdministrationRest extends BasicRest {
  dynamic getUserInfo(BuildContext context) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      String? token = prefs.getString('token');
      Response response = await Dio().get(
        'http://localhost:8092/user',
        options: Options(
          headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            'Content-Type': 'application/json; charset=UTF-8',
            "Access-Control_Allow_Origin": "*",
            'Authorization': 'Bearer ' + token!,
          },
        ),
      );
      return response.data;
    } on DioError catch (e) {
      await checkErrorOrAuth(e, context);
    }
  }

  Future<int> createUser(String login, String email, String password,
      BuildContext context) async {
    try {
      Response response = await Dio().put(
        // 'http://localhost:8061/user',
        'http://localhost:8092/user',
        data: {'login': login, 'email': email, 'password': password},
        options: Options(
          headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            'Content-Type': 'application/json; charset=UTF-8',
            "Access-Control_Allow_Origin": "*"
          },
        ),
      );
      return response.statusCode!;
    } on DioError catch (e) {
      await checkErrorOrAuthWithErrors(e, context);
    }
    return 0;
  }
}
