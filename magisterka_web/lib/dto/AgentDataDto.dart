class AgentDataDto {
  late String _messageUid;
  late String _messageData;
  late Map _data;

  String get messageUid => _messageUid;

  set messageUid(String value) {
    _messageUid = value;
  }

  Map get data => _data;

  set data(Map value) {
    _data = value;
  }

  String get messageData => _messageData;

  set messageData(String value) {
    _messageData = value;
  }


  AgentDataDto(this._messageUid, this._messageData, this._data);

  AgentDataDto.fromJson(Map<String, dynamic> json)
      : _messageUid = json['messageUid'],
        _messageData = json['messageDate'],
        _data = json['data'];

  Map<String, dynamic> toJson() =>
      {'messageUid': _messageUid, 'data': _data, "messageDate": _messageData};
} 
