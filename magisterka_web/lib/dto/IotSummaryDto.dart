import 'dart:convert';

IotSummaryDto iotSummaryDtoFromJson(String str) =>
    IotSummaryDto.fromJson(json.decode(str));

String iotSummaryDtoToJson(IotSummaryDto data) => json.encode(data.toJson());

class IotSummaryDto {
  IotSummaryDto.fromJson(dynamic json) {
    _agentNumber = json['agentNumber'];
    _ruleNumber = json['ruleNumber'];
    _lastHourData = json['lastHourData'];
    _lastDayData = json['lastDayData'];
    _dataNumber = json['dataNumber'];
    _topicNumber = json['topicNumber'];
  }

  late int _agentNumber;
  late int _ruleNumber;
  late int _lastHourData;
  late int _lastDayData;
  late int _dataNumber;
  late Map _topicNumber;

  int get agentNumber => _agentNumber;

  int get ruleNumber => _ruleNumber;

  int get lastHourData => _lastHourData;

  int get lastDayData => _lastDayData;

  int get dataNumber => _dataNumber;

  Map get topicNumber => _topicNumber;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['agentNumber'] = _agentNumber;
    map['ruleNumber'] = _ruleNumber;
    map['lastHourData'] = _lastHourData;
    map['lastDayData'] = _lastDayData;
    map['dataNumber'] = _dataNumber;
    if (_topicNumber != null) {
      map['topicNumber'] = _topicNumber;
    }
    return map;
  }



  IotSummaryDto(this._agentNumber, this._ruleNumber, this._lastHourData,
      this._lastDayData, this._dataNumber, this._topicNumber);
}
