class AgentRule {
  late String _ruleUid;
  late String _ruleCode;
  late String _ruleConfig;

  String get ruleUid => _ruleUid;

  set ruleUid(String value) {
    _ruleUid = value;
  }

  String get ruleCode => _ruleCode;

  String get ruleConfig => _ruleConfig;

  set ruleConfig(String value) {
    _ruleConfig = value;
  }

  set ruleCode(String value) {
    _ruleCode = value;
  }

  AgentRule(this._ruleUid, this._ruleCode, this._ruleConfig);

  AgentRule.fromJson(Map<String, dynamic> json)
      : _ruleUid = json['ruleUid'],
        _ruleCode = json['ruleCode'],
        _ruleConfig = json['ruleConfig'];

  Map<String, dynamic> toJson() =>
      {'ruleUid': _ruleUid, 'ruleCode': _ruleCode, 'ruleConfig': _ruleConfig};
}
