class AgentExtDto {
  late String _agentUid;
  late String _agentName;
  late String _agentTypeCode;

  String get agentUid => _agentUid;

  set agentUid(String value) {
    _agentUid = value;
  }

  String get agentName => _agentName;

  String get agentTypeCode => _agentTypeCode;

  set agentTypeCode(String value) {
    _agentTypeCode = value;
  }

  set agentName(String value) {
    _agentName = value;
  }

  AgentExtDto(this._agentUid, this._agentName, this._agentTypeCode);

  AgentExtDto.fromJson(Map<String, dynamic> json)
      : _agentUid = json['agentUid'],
        _agentName = json['agentName'],
        _agentTypeCode = json['agentTypeCode'];

  Map<String, dynamic> toJson() => {
        'agentUid': _agentUid,
        'agentName': _agentName,
        'agentTypeCode': _agentTypeCode
      };
}
