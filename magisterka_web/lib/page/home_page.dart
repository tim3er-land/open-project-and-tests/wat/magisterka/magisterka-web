import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:magisterka_web/page/fragment/rule_fragment.dart';
import 'package:magisterka_web/page/fragment/send_fragment.dart';
import 'package:magisterka_web/page/fragment/user_fragment.dart';

import 'fragment/agent_fragment.dart';
import 'fragment/home_fragment.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedIndex = 1; //New
  late PageController _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  final List<Widget> _mainContents = [
    Container(
      child: HomeFragment(),
    ),
    Container(
      child: AgentFragment(),
    ),
    Container(
      child: RuleFragment(),
    ),
    Container(
      child: SendFragment(),
    ),
    Container(
      child: UserFragment(),
    )
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.startFloat,
      floatingActionButton: Visibility(
        child: FloatingActionButton(
          child: new Icon(Icons.add),
          onPressed: () {
            if (_selectedIndex == 1) {
              Navigator.pushNamed(context, '/agent/add', arguments: {});
            } else {
              Navigator.pushNamed(context, '/rule/add', arguments: {});
            }
          },
        ),
        visible: _selectedIndex == 1 || _selectedIndex == 2,
      ),
      body: Row(
        children: [
          NavigationRail(
            extended: true,

            destinations: const <NavigationRailDestination>[
              NavigationRailDestination(
                icon: Icon(Icons.home),
                label: Text('Home'),
              ),
              NavigationRailDestination(
                icon: Icon(Icons.sensors),
                label: Text('Agent'),
              ),
              NavigationRailDestination(
                icon: Icon(Icons.rule),
                label: Text('Rules'),
              ),
              NavigationRailDestination(
                icon: Icon(Icons.send),
                label: Text('Send'),
              ),
              NavigationRailDestination(
                icon: Icon(Icons.account_circle),
                label: Text('User'),
              ),
            ],
            selectedIndex: _selectedIndex,
            onDestinationSelected: (int index) {
              setState(() {
                _selectedIndex = index;
              });
            },
          ),
          VerticalDivider(),
          Expanded(
            child: _mainContents[_selectedIndex],
          ),
        ],
      ),
    );
  }

  void onTabTapped(int index) {
    setState(() {
      _selectedIndex = index;
      _pageController.jumpToPage(_selectedIndex);
    });
  }
}
