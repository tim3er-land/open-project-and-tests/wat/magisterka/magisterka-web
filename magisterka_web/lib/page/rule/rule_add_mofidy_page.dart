import 'dart:convert';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:magisterka_web/rest/iot_administration_rest.dart';

class RuleAddModifyPage extends StatefulWidget {
  Map arguments;
  bool agentEdith;

  RuleAddModifyPage(this.arguments, this.agentEdith);

  @override
  State<RuleAddModifyPage> createState() =>
      _RuleAddModifyPageState(arguments, agentEdith);
}

class _RuleAddModifyPageState extends State<RuleAddModifyPage> {
  Map arguments;
  bool agentEdith;
  late String _ruleConfig;
  late String _ruleCode;
  late Map _ruleMap;

  _RuleAddModifyPageState(this.arguments, this.agentEdith);

  @override
  Widget build(BuildContext context) {
    final _formKey = GlobalKey<FormState>();
    List<DropdownMenuItem<String>> menuItems = [
      DropdownMenuItem(
          child: Text("Save processed data"), value: "SAVE_PROCESSED_DATA"),
      DropdownMenuItem(child: Text("Redirect data"), value: "REDIRECT_DATA"),
      DropdownMenuItem(child: Text("Save raw data"), value: "SAVE_RAW_DATA")
    ];
    if (agentEdith) {
      _ruleCode = arguments['ruleCode'];
      Map<String, dynamic> data = json.decode(arguments['ruleConfig']);
      _ruleConfig = data['config'];
    } else {
      _ruleCode = menuItems[0].value!;
      _ruleConfig = '';
    }
    return Scaffold(
        appBar:
            AppBar(title: agentEdith ? Text("Edith rule") : Text("Add rule")),
        body: Container(
          padding: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width * 0.3),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              DropdownButton(
                value: _ruleCode,
                items: menuItems,
                onChanged: (String? value) => _ruleCode = value!,
              ),
              Form(
                key: _formKey,
                child: TextFormField(
                    initialValue: _ruleConfig,
                    decoration: InputDecoration(hintText: "rule config"),
                    onChanged: (value) {
                      _ruleConfig = value;
                      _ruleMap = {'config': value};
                    },
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter rule config ';
                      }
                      return null;
                    }),
              ),
              SizedBox(
                height: 30,
              ),
              OutlinedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (agentEdith)
                        await IotAdministrationRest().modifyAgentRule(
                            context,
                            {
                              'ruleConfig': json.encode(_ruleMap),
                              'ruleCode': _ruleCode
                            },
                            arguments['ruleUid']);
                      else
                        await IotAdministrationRest().addNewRule(context, {
                          'ruleConfig': json.encode(_ruleMap),
                          'ruleCode': _ruleCode
                        });
                    }
                  },
                  child: agentEdith ? Text("Update rule") : Text("Save rule"))
            ],
          ),
        ));
  }
}
