import 'package:flutter/material.dart';
import 'package:magisterka_web/rest/user_administration_rest.dart';

import '../rest/auth_rest.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  late String _login;
  late String _email;
  late String _password;

  @override
  Widget build(BuildContext context) {
    final _formKey = GlobalKey<FormState>();

    return Scaffold(
      body: Container(
        padding: EdgeInsets.symmetric(
            horizontal: (MediaQuery.of(context).size.width * 0.3)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: 30,
            ),
            Form(
                key: _formKey,
                child: Column(
                  children: [
                    TextFormField(
                      onChanged: (value) => _login = value,
                      decoration: new InputDecoration(hintText: "Login"),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter login';
                        }
                        return null;
                      },
                    ),
                    TextFormField(
                      // key: _formKey,
                      onChanged: (value) => _email = value,
                      decoration: new InputDecoration(hintText: "Email"),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter email';
                        }
                        return null;
                      },
                    ),
                    TextFormField(
                      // key: _formKey,
                      onChanged: (value) => _password = value,
                      obscureText: true,
                      decoration: new InputDecoration(
                        hintText: "Password",
                      ),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter passoword';
                        }
                        return null;
                      },
                    ),
                  ],
                )),
            SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                OutlinedButton(
                    onPressed: () async {
                      if (_formKey.currentState!.validate()) {
                        int status = await UserAdministrationRest()
                            .createUser(_login, _email, _password, context);
                        if (status == 201) {
                          AuthRest().login(_password, _login, context);
                        }
                      }
                    },
                    child: Text("Register")),
                SizedBox(
                  width: 30,
                ),
                OutlinedButton(
                    onPressed: () =>
                        Navigator.pushReplacementNamed(context, "/"),
                    child: Text("Back"))
              ],
            )
          ],
        ),
      ),
    );
  }
}
