import 'package:flutter/material.dart';
import 'package:magisterka_web/rest/auth_rest.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  var _login;
  var _password;

  @override
  Widget build(BuildContext context) {
    final _formKey = GlobalKey<FormState>();

    return Scaffold(
      body: Container(
        padding: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width * 0.3),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Form(
                key: _formKey,
                child: Column(
                  children: [
                    TextFormField(
                      onChanged: (value) => _login = value,
                      decoration: new InputDecoration(hintText: "Login"),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter login';
                        }
                        return null;
                      },
                    ),
                    TextFormField(
                      obscureText: true,
                      onChanged: (value) => _password = value,
                      decoration: new InputDecoration(hintText: "Password"),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter password';
                        }
                        return null;
                      },
                    ),
                  ],
                )),
            SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                OutlinedButton(
                    onPressed: () {
                      if (_formKey.currentState!.validate())
                        AuthRest().login(_password, _login, context);
                    },
                    child: Text("Login")),
                SizedBox(
                  width: 30,
                ),
                OutlinedButton(
                    onPressed: () =>
                        Navigator.pushReplacementNamed(context, "/"),
                    child: Text("Back"))
              ],
            )
          ],
        ),
      ),
    );
  }
}
