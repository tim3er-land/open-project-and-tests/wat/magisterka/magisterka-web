import 'package:flutter/material.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:shared_preferences/shared_preferences.dart';

class WelcomePage extends StatelessWidget {
  const WelcomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Strona projektu do pracy magisterskiej",
                style: TextStyle(fontSize: 50),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                  "Projektowanie i implementacja systemu współpracujących obiektów Internetu Rzeczy z wykorzystaniem środowiska agentowego JADE"),
              Text("Temat pracy",
                  style: TextStyle(fontSize: 15, color: Colors.grey.shade500)),
              Text("inż. Piotr Witowski"),
              Text("Autor",
                  style: TextStyle(fontSize: 15, color: Colors.grey.shade500)),
              Text("dr hab. inż. Zbigniew Zieliński prof. WAT"),
              Text("Promotor",
                  style: TextStyle(fontSize: 15, color: Colors.grey.shade500)),
              SizedBox(
                height: 30,
              ),
              Row(
                children: [
                  OutlinedButton(
                      onPressed: () async {
                        final prefs = await SharedPreferences.getInstance();
                        String? token = prefs.getString('token');
                        var path = token == null || JwtDecoder.isExpired(token)
                            ? "/login"
                            : '/home';
                        Navigator.pushReplacementNamed(context, path);
                      },
                      child: Text("Login")),
                  SizedBox(
                    width: 30,
                  ),
                  OutlinedButton(
                      onPressed: () {
                        Navigator.pushReplacementNamed(context, "/register");
                      },
                      child: Text("Rejestracja"))
                ],
              )
            ],
          ),
        ],
      ),
    );
  }
}
