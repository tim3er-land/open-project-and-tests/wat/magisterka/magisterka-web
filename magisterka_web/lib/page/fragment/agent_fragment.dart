import 'dart:convert';
import 'dart:html';
import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:url_launcher_web/url_launcher_web.dart';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:magisterka_web/dto/AgentExtDto.dart';
import 'package:magisterka_web/rest/iot_administration_rest.dart';
import 'package:path_provider/path_provider.dart';

class AgentFragment extends StatefulWidget {
  const AgentFragment({Key? key}) : super(key: key);

  @override
  State<AgentFragment> createState() => _AgentFragmentState();
}

class _AgentFragmentState extends State<AgentFragment> {
  bool _loading = false;

  @override
  Widget build(BuildContext context) {
    // _loading ? return SpinKitDualRing(color: Colors.black12):
    return Container(
      child: FutureBuilder(
        future: IotAdministrationRest().getUserIotAgents(context),
        builder:
            (BuildContext context, AsyncSnapshot<List<AgentExtDto>> snapshot) {
          if (snapshot.connectionState == ConnectionState.done &&
              snapshot.hasData) {
            return Container(
              child: ListView(
                  children:
                      snapshot.data!.map((e) => getUserIotAgents(e)).toList()),
              // child: getUserIotAgents(snapshot.data),
            );
          }
          return SpinKitDualRing(color: Colors.blue.shade300);
        },
      ),
    );
  }

  Widget getUserIotAgents(AgentExtDto data) {
    return Column(children: [
      SizedBox(
        height: 10,
      ),
      Card(
        margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: Column(
          children: [
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  children: [
                    Text(data.agentName),
                    Text(
                      'Agent name',
                      style:
                          TextStyle(fontSize: 15, color: Colors.grey.shade500),
                    )
                  ],
                ),
                Column(
                  children: [
                    Text(data.agentUid),
                    Text('agent id',
                        style: TextStyle(
                            fontSize: 15, color: Colors.grey.shade500))
                  ],
                )
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                OutlinedButton(
                    onPressed: () => Navigator.pushNamed(context, "/agent/info",
                        arguments: {'agentUid': data.agentUid}),
                    child: Text("Agent info")),
                OutlinedButton(
                    onPressed: () {
                      getAgentZip(data.agentUid);
                    },
                    child: Text("Agent zip")),
                OutlinedButton(
                    onPressed: () => Navigator.pushNamed(context, "/agent/data",
                        arguments: {'agentUid': data.agentUid}),
                    child: Text("Agent data")),
                OutlinedButton(
                    onPressed: () async {
                      await IotAdministrationRest()
                          .deleteAgent(context, data.agentUid);
                      setState(() {});
                      ;
                    },
                    child: Text("Delete agent"))
              ],
            ),
            SizedBox(
              height: 10,
            ),
          ],
        ),
      )
    ]);
  }

  void getAgentInfo() {}

  void getAgentZip(String agentUid) async {
    if (kIsWeb) {
      final String encodedStr =
          await IotAdministrationRest().getAgentZip(context, agentUid);
      download(encodedStr.codeUnits, agentUid + ".zip");
    }
  }

  void download(
    List<int> bytes,
    String downloadName,
  ) {
    // Encode our file in base64
    final _base64 = base64Encode(bytes);
    // Create the link with the file
    final anchor =
        AnchorElement(href: 'data:application/octet-stream;base64,$_base64')
          ..target = 'blank';
    // add the name
    if (downloadName != null) {
      anchor.download = downloadName;
    }
    // trigger download
    document.body?.append(anchor);
    anchor.click();
    anchor.remove();
    return;
  }

  void getAgentData() {}
}
