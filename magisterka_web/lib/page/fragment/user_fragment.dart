import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:magisterka_web/rest/user_administration_rest.dart';
import 'package:magisterka_web/utils/mgr_date_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserFragment extends StatefulWidget {
  const UserFragment({Key? key}) : super(key: key);

  @override
  State<UserFragment> createState() => _UserFragmentState();
}

class _UserFragmentState extends State<UserFragment> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          FutureBuilder(
            future: getUserInfo(context),
            builder: (context, AsyncSnapshot<dynamic> snapshot) {
              if (snapshot.connectionState == ConnectionState.done &&
                  snapshot.hasData) {
                return Container(
                  child: getUserInfoWidget(snapshot.data),
                );
              }
              return SpinKitDualRing(color: Colors.blue.shade300);
            },
          ),
          OutlinedButton(
              onPressed: () async {
                final prefs = await SharedPreferences.getInstance();
                await prefs.remove('token');
                Navigator.pushReplacementNamed(context, '/');
              },
              child: Text("Logout"))
        ],
      ),
    );
  }

  Future<dynamic> getUserInfo(BuildContext context) async {
    return await UserAdministrationRest().getUserInfo(context);
  }

  getUserInfoWidget(Map<dynamic, dynamic> map) {
    return Column(
      children: [
        Text(map['login']),
        Text('login',
            style: TextStyle(color: Colors.grey.shade500, fontSize: 15)),
        SizedBox(
          height: 10,
        ),
        Text(map['email']),
        Text('email',
            style: TextStyle(color: Colors.grey.shade500, fontSize: 15)),
        SizedBox(
          height: 10,
        ),
        Text(map['lastLogging'] == null ? "" : MgrDateUtils.parseTime(map['lastLogging'])),
        Text('lastLogging',
            style: TextStyle(color: Colors.grey.shade500, fontSize: 15)),
        SizedBox(
          height: 10,
        ),
        Text(map['lastErrorLogging'] == null
            ? ""
            : MgrDateUtils.parseTime(map['lastErrorLogging'])),
        Text('lastErrorLogging',
            style: TextStyle(color: Colors.grey.shade500, fontSize: 15))
      ],
    );
  }
}

class UserInfoDto {}
