import 'package:flutter/material.dart';
import 'package:magisterka_web/rest/iot_data_rest.dart';

class SendFragment extends StatefulWidget {
  const SendFragment({Key? key}) : super(key: key);

  @override
  State<SendFragment> createState() => _SendFragmentState();
}

class _SendFragmentState extends State<SendFragment> {
  late String _topic;
  late String _data;

  @override
  Widget build(BuildContext context) {
    final _formKey = GlobalKey<FormState>();

    return Container(
      padding: EdgeInsets.symmetric(
          horizontal: MediaQuery.of(context).size.width * 0.2),
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextFormField(
                initialValue: '',
                decoration: InputDecoration(hintText: "topic"),
                onChanged: (value) => _topic = value,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter topic';
                  }
                  return null;
                }),
            TextFormField(
                initialValue: '',
                decoration: InputDecoration(hintText: "data"),
                onChanged: (value) => _data = value,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter data';
                  }
                  return null;
                }),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.05,
            ),
            OutlinedButton(
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    IotDataRest().sendDataToAgent(context, _topic, _data);
                  }
                },
                child: Text('Send'))
          ],
        ),
      ),
    );
  }
}
