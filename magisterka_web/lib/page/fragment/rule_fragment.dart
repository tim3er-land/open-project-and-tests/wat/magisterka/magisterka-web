import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:magisterka_web/dto/AgentTule.dart';
import 'package:magisterka_web/rest/iot_administration_rest.dart';

import '../../dto/AgentExtDto.dart';

class RuleFragment extends StatefulWidget {
  const RuleFragment({Key? key}) : super(key: key);

  @override
  State<RuleFragment> createState() => _RuleFragmentState();
}

class _RuleFragmentState extends State<RuleFragment> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: IotAdministrationRest().getUserRule(context),
      builder: (context, AsyncSnapshot<List<AgentRule>> snapshot) =>
          generateRuleCards(context, snapshot),
    );
  }

  Widget generateRuleCards(
      BuildContext context, AsyncSnapshot<List<AgentRule>> snapshot) {
    if (snapshot.connectionState == ConnectionState.done && snapshot.hasData)
      return Column(
          children:
              snapshot.data!.map((e) => generateRuleCard(e, context)).toList());

    return SpinKitDualRing(
      color: Colors.grey.shade500,
    );
  }

  Future<List<AgentRule>> getUserRules(BuildContext context) async {
    return await new IotAdministrationRest().getUserRule(context);
  }

  Widget generateRuleCard(AgentRule e, BuildContext context) {
    Map<String, dynamic> data = json.decode(e.ruleConfig);

    return Card(
      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
        child: Column(
          children: [
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  children: [
                    Text(e.ruleCode),
                    Text("Rule code",
                        style: TextStyle(
                            color: Colors.grey.shade500, fontSize: 15))
                  ],
                ),
                Column(
                  children: [
                    Text(data['config']),
                    Text("Rule Config",
                        style: TextStyle(
                            color: Colors.grey.shade500, fontSize: 15))
                  ],
                )
              ],
            ),
            SizedBox(height: 20,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                OutlinedButton(
                    onPressed: () async {
                      await IotAdministrationRest()
                          .deleteUserRule(context, e.ruleUid);
                      setState(() {});
                    },
                    child: Text("Delete Rule")),
                OutlinedButton(
                    onPressed: () => Navigator.pushNamed(context, '/rule/edith',
                            arguments: {
                              'ruleUid': e.ruleUid,
                              'ruleConfig': e.ruleConfig,
                              'ruleCode': e.ruleCode
                            }),
                    child: Text("Modify Rule"))
              ],
            ),
            SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
    );
  }
}
