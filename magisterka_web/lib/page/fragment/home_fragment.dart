import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:magisterka_web/rest/iot_data_rest.dart';

import '../../dto/IotSummaryDto.dart';

class HomeFragment extends StatefulWidget {
  const HomeFragment({Key? key}) : super(key: key);

  @override
  State<HomeFragment> createState() => _HomeFragmentState();
}

class _HomeFragmentState extends State<HomeFragment> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: getIotSummary(context),
        builder:
            (BuildContext context, AsyncSnapshot<IotSummaryDto> snapshot) =>
                prepareHomeFragment(context, snapshot));
  }

  Stream<IotSummaryDto> getIotSummary(BuildContext context) async* {
    while (true) {
      await Future.delayed(Duration(milliseconds: 5000));
      IotSummaryDto iotSummaryDto = await IotDataRest().getIotSummary(context);
      yield iotSummaryDto;
    }
  }

  prepareHomeFragment(
      BuildContext context, AsyncSnapshot<IotSummaryDto> snapshot) {
    if (snapshot.hasData) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          new Card(
            child: new Column(
              children: <Widget>[
                Text("Basic info",
                    style: TextStyle(
                        fontSize: 25,
                        color: Colors.black87,
                        fontWeight: FontWeight.w900)),
                new Padding(
                    padding: new EdgeInsets.all(7.0),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Column(
                          children: [
                            Text(
                              snapshot.data!.agentNumber.toString(),
                              style: TextStyle(fontSize: 25),
                            ),
                            Text(
                              'agentNumber',
                              style: TextStyle(
                                  color: Colors.grey.shade500, fontSize: 15),
                            ),
                          ],
                        ),
                        Column(
                          children: [
                            Text(snapshot.data!.ruleNumber.toString(),
                                style: TextStyle(fontSize: 25)),
                            Text('ruleNumber',
                                style: TextStyle(
                                    color: Colors.grey.shade500, fontSize: 15)),
                          ],
                        )
                      ],
                    ))
              ],
            ),
          ),
          SizedBox(
            height: 20,
          ),
          new Card(
            child: new Column(
              children: <Widget>[
                Text(
                  "Data info",
                  style: TextStyle(
                      fontSize: 25,
                      color: Colors.black87,
                      fontWeight: FontWeight.w900),
                ),
                new Padding(
                    padding: new EdgeInsets.all(7.0),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Column(
                          children: [
                            Text(snapshot.data!.dataNumber.toString(),
                                style: TextStyle(fontSize: 25)),
                            Text('dataNumber',
                                style: TextStyle(
                                    color: Colors.grey.shade500, fontSize: 15)),
                          ],
                        ),
                        Column(
                          children: [
                            Text(snapshot.data!.lastHourData.toString(),
                                style: TextStyle(fontSize: 25)),
                            Text('lastHourData',
                                style: TextStyle(
                                    color: Colors.grey.shade500, fontSize: 15)),
                          ],
                        ),
                        Column(
                          children: [
                            Text(snapshot.data!.lastDayData.toString(),
                                style: TextStyle(fontSize: 25)),
                            Text('lastDayData',
                                style: TextStyle(
                                    color: Colors.grey.shade500, fontSize: 15)),
                          ],
                        )
                      ],
                    ))
              ],
            ),
          ),
        ],
      );
    }
    return SpinKitDualRing(color: Colors.blue);
  }
}
