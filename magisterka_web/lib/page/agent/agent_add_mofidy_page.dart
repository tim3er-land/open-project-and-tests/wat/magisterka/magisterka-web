import 'package:flutter/material.dart';
import 'package:magisterka_web/rest/iot_administration_rest.dart';

class AgentAddModifyPage extends StatefulWidget {
  Map arguments;
  bool agentEdith;

  AgentAddModifyPage(this.arguments, this.agentEdith);

  @override
  State<AgentAddModifyPage> createState() =>
      _AgentAddModifyPageState(arguments, agentEdith);
}

class _AgentAddModifyPageState extends State<AgentAddModifyPage> {
  Map arguments;
  bool agentEdith;
  late String _agentName;

  _AgentAddModifyPageState(this.arguments, this.agentEdith);

  @override
  Widget build(BuildContext context) {
    final _formKey = GlobalKey<FormState>();

    return Scaffold(
        appBar:
            AppBar(title: agentEdith ? Text("Edith agent") : Text("Add agent")),
        body: Container(
          padding: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width * 0.3),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Form(
                child: TextFormField(
                    key: _formKey,
                    initialValue: agentEdith ? arguments['agentName'] : '',
                    decoration: InputDecoration(hintText: "agentName"),
                    onChanged: (value) => _agentName = value,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter agent name ';
                      }
                      return null;
                    }),
              ),
              OutlinedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      await IotAdministrationRest().addNewAgent(context,
                          {'agentName': _agentName, 'agentTypeCode': ''});
                    }
                  },
                  child: agentEdith ? Text("Update agent") : Text("Save agent"))
            ],
          ),
        ));
  }
}
