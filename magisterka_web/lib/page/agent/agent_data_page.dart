import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:magisterka_web/utils/mgr_date_utils.dart';

import '../../dto/AgentDataDto.dart';
import '../../rest/iot_data_rest.dart';

class AgentDataPage extends StatefulWidget {
  Map arguments;

  AgentDataPage(this.arguments);

  @override
  State<AgentDataPage> createState() => _AgentDataFragmentState(arguments);
}

class _AgentDataFragmentState extends State<AgentDataPage> {
  Map arguments;

  _AgentDataFragmentState(this.arguments);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(arguments['agentUid'] + " data"),
      ),
      body: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          SingleChildScrollView(
            child: Column(children: [
              SizedBox(
                height: 20,
              ),
              Text("Processd data", style: TextStyle(fontSize: 25)),
              SizedBox(
                height: 30,
              ),
              StreamBuilder(
                stream: agentDataStream(context, arguments['agentUid']),
                initialData: <AgentDataDto>[AgentDataDto('', '', {})],
                builder: (BuildContext context,
                        AsyncSnapshot<List<AgentDataDto>> snapshot) =>
                    prepareDataCards(context, snapshot),
              )
            ]),
          ),
          SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  height: 20,
                ),
                Text("Raw data", style: TextStyle(fontSize: 25)),
                SizedBox(
                  height: 30,
                ),
                StreamBuilder(
                  stream: agentDataStreamRaw(context, arguments['agentUid']),
                  initialData: <AgentDataDto>[AgentDataDto('', '', {})],
                  builder: (BuildContext context,
                          AsyncSnapshot<List<AgentDataDto>> snapshot) =>
                      prepareDataCardsRaw(context, snapshot),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Stream<List<AgentDataDto>> agentDataStream(
      BuildContext context, String agentUid) async* {
    while (true) {
      await Future.delayed(Duration(milliseconds: 5000));
      List<AgentDataDto> dataMap =
          await IotDataRest().getAgentData(context, agentUid);
      yield dataMap;
    }
  }

  Stream<List<AgentDataDto>> agentDataStreamRaw(
      BuildContext context, String agentUid) async* {
    while (true) {
      await Future.delayed(Duration(milliseconds: 5000));
      List<AgentDataDto> dataMap =
          await IotDataRest().getAgentDataRaw(context, agentUid);
      yield dataMap;
    }
  }

  Widget prepareDataCards(
      BuildContext context, AsyncSnapshot<List<AgentDataDto>> snapshot) {
    if (snapshot.hasData)
      return Column(
        children: snapshot.data!
            .map((e) => Card(
                  child: Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: (MediaQuery.of(context).size.width * .1),
                      vertical: (MediaQuery.of(context).size.height * .01),
                    ),
                    child: Column(
                      children: [
                        // Row(
                        //   children: [
                        //     Text('messageUid: '),
                        //     Text(e.messageUid),
                        //   ],
                        // ),
                        Row(
                          children: [
                            Text('messageData: '),
                            Text(MgrDateUtils.parseTime(e.messageData)),
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Column(
                          children: [
                            Text(
                              'data',
                            ),
                            Column(
                              children: extractDataFromMap(e.data),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ))
            .toList(),
      );
    return SpinKitDualRing(color: Colors.black);
  }

  Widget prepareDataCardsRaw(
      BuildContext context, AsyncSnapshot<List<AgentDataDto>> snapshot) {
    if (snapshot.hasData)
      return Column(
        children: snapshot.data!
            .map((e) => Card(
                  child: Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: (MediaQuery.of(context).size.width * .1),
                      vertical: (MediaQuery.of(context).size.height * .01),
                    ),
                    child: Column(
                      children: [
                        // Row(
                        //   children: [
                        //     Text('messageUid: '),
                        //     Text(e.messageUid),
                        //   ],
                        // ),
                        Row(
                          children: [
                            Text('messageData: '),
                            Text(MgrDateUtils.parseTime(e.messageData)),
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Column(
                          children: [
                            Text(
                              'data',
                            ),
                            Column(
                              children: extractDataFromMap(e.data),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ))
            .toList(),
      );
    return SpinKitDualRing(color: Colors.black);
  }

  String getPrettyJSONString(jsonObject) {
    var encoder = new JsonEncoder.withIndent("     ");
    return encoder.convert(jsonObject);
  }

  List<Widget> extractDataFromMap(Map<dynamic, dynamic> data) {
    List<Widget> datas = [];
    for (var k in data.keys) {
      datas.add(Row(
        children: [
          Text(k),
          SizedBox(
            width: 10,
          ),
          Text(data[k].toString())
        ],
      ));
    }
    return datas;
  }
}
