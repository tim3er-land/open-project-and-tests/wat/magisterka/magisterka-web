import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:magisterka_web/rest/iot_administration_rest.dart';
import 'package:magisterka_web/utils/mgr_date_utils.dart';

class AgentInfoPage extends StatefulWidget {
  Map arguments;

  AgentInfoPage(this.arguments);

  @override
  State<AgentInfoPage> createState() => _AgentInfoPageState(arguments);
}

class _AgentInfoPageState extends State<AgentInfoPage> {
  Map arguments;

  _AgentInfoPageState(this.arguments);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          FutureBuilder(
            future: IotAdministrationRest()
                .getAgentInfo(context, arguments['agentUid']),
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) =>
                generateAgentInfoWidget(context, snapshot),
          )
        ],
      ),
      appBar: AppBar(
        title: Text(arguments['agentUid'] + " info"),
      ),
    );
  }

  generateAgentInfoWidget(
      BuildContext context, AsyncSnapshot<dynamic> snapshot) {
    if (snapshot.hasData) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Column(
            children: [
              Text(snapshot.data['agentName']),
              Text("agent Name",
                  style: TextStyle(color: Colors.grey.shade500, fontSize: 15)),
              SizedBox(
                height: 10,
              ),
              Text(snapshot.data['agentUid']),
              Text("agent uid",
                  style: TextStyle(color: Colors.grey.shade500, fontSize: 15)),
              SizedBox(
                height: 10,
              ),
              Text(MgrDateUtils.parseTime(snapshot.data['agentModifyDate'])),
              Text("agent last modify",
                  style: TextStyle(color: Colors.grey.shade500, fontSize: 15)),
            ],
          ),
          Column(
            children: [
              Text(
                "Agent topic",
                style: TextStyle(
                    fontSize: 25,
                    color: Colors.black87,
                    fontWeight: FontWeight.w900),
              ),
              SizedBox(height: 20,),
              Column(
                children: snapshot.data['agentTopics'] == null
                    ? <Text>[Text("")]
                    : snapshot.data['agentTopics']
                        .map<Widget>((s) => Text(s))
                        .toList(),
              )
            ],
          )
        ],
      );
    }
    return SpinKitDualRing(color: Colors.black12);
  }
}
